from django.urls import path
from . import views

urlpatterns = [
    path('<int:id>',views.detail, name="meeting_detail"),
    path('rooms',views.rooms, name="rooms"),
    path('new',views.new, name="new_meeting"),
]
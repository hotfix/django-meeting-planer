from pyexpat import model
from django.db import models

from datetime import time
from django.core.validators import MinValueValidator, MaxValueValidator
# Create your models here.

class Room(models.Model):
    name = models.CharField(max_length=100)
    floor_number = models.PositiveSmallIntegerField(validators=[
                MaxValueValidator(3),
                MinValueValidator(0)
            ])
    room_number = models.PositiveSmallIntegerField(validators=[
                MaxValueValidator(10),
                MinValueValidator(1)
            ])

    def __str__(self):
        return f"{self.name} #{self.room_number} on {self.floor_number} floor"


class Meeting(models.Model):
    title = models.CharField(max_length=200)
    date = models.DateField()
    start_time = models.TimeField(default=time(9,0))
    duration = models.IntegerField(default=1)
    room = models.ForeignKey(Room,on_delete=models.CASCADE)

    def __str__(self):
        return f"{self.title} at {self.start_time} on {self.date}"



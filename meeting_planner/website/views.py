
from django.shortcuts import render
from django.http import HttpResponse
from datetime import datetime
from meetings.models import Meeting

# Create your views here.
def welcome(request):
    meetings = Meeting.objects.all()
    return render(request, "website/welcome.html"
                ,{"meetings": meetings})

def date(request):
    return HttpResponse("This page was served at " + datetime.now().strftime("%d.%m.%Y, %H:%M:%S"))


def about(request):
    return HttpResponse("Hello, my  name is Alex!")